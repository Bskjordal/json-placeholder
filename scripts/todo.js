window.onload = init;

function init() {
const btn = document.getElementById("subBtn");
btn.onclick = CallAPI;

};

function CallAPI() {
    
let msgDiv = document.getElementById("msgDiv");
let userId  = document.getElementById("inputBox").value;
let url     = "https://jsonplaceholder.typicode.com/todos/"  + userId;
fetch(url)
  .then((response) => response.json())
  .then((data) => {
    let message = "UserId: " + data.userId + " Title: " + data.title;
    msgDiv.innerHTML = message;
  });

}