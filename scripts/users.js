window.onload = CallAPI;

function CallAPI() {

    let table = document.getElementById("UsrTblBody");
    let url = "https://jsonplaceholder.typicode.com/users";

    fetch(url)
        .then(response => response.json())
        .then(data => {
            for (let i = 0; i < data.length; i++) {
                let row = table.insertRow(-1);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(1);
                let cell3 = row.insertCell(2);
                let cell4 = row.insertCell(3);
                let cell5 = row.insertCell(4);
                cell1.innerHTML = data[i].name;
                cell2.innerHTML = data[i].email;
                cell3.innerHTML = data[i].address.street + " " + " " + data[i].address.suite + " " + data[i].address.city + " " + data[i].address.zipcode;
                cell4.innerHTML = data[i].phone;
                cell5.innerHTML = data[i].website;
            }
        
        })}